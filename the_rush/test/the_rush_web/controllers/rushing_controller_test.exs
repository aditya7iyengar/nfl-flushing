defmodule TheRushWeb.RushingControllerTest do
  use TheRushWeb.ConnCase

  alias TheRush.Factory

  describe "GET /api/rushings" do
    setup %{conn: conn} = context do
      rushings = Enum.map(1..25, fn _ -> Factory.insert(:rushing) end)
      route = Routes.rushing_path(conn, :index)
      params = context[:params]

      conn = get(conn, route, params)

      {
        :ok,
        rushings: rushings, response_conn: conn
      }
    end

    @tag params: %{}
    test "success: lists first 10 (default) rushings",
         %{response_conn: conn, rushings: rushings} do
      # ok response
      assert conn.status == 200

      %{
        "data" => results,
        "page_info" => page_info
      } = json_response(conn, 200)

      assert length(results) == 10

      Enum.with_index(results, fn result, index ->
        expected_rushing = Enum.at(rushings, index)

        for key <- Map.keys(result) do
          atom_key = String.to_atom(key)
          actual = Map.get(result, key)
          expected = Map.get(expected_rushing, atom_key)

          assert(
            to_string(actual) == to_string(expected),
            "Expected #{key} to be #{inspect(expected)}, got #{inspect(actual)}"
          )
        end
      end)

      assert page_info == %{
               "page" => 1,
               "page_size" => 10,
               "total_pages" => ceil(25 / 10)
             }
    end

    @tag params: %{sort_by: :bad_sort}
    test "failure: returns errors", %{response_conn: conn} do
      assert conn.status == 422
      %{"errors" => errors} = json_response(conn, 422)

      assert length(errors) == 1
      [error] = errors
      assert error == %{"field" => "sort_by", "message" => "is invalid"}
    end
  end
end

defmodule TheRushWeb.RushingExportControllerTest do
  use TheRushWeb.ConnCase, async: false

  alias TheRush.Factory

  describe "GET /api/rushing_exports" do
    setup %{conn: conn} = context do
      rushing_exports = Enum.map(1..25, fn _ -> Factory.insert(:rushing_export) end)
      route = Routes.rushing_export_path(conn, :index)
      params = context[:params]

      conn = get(conn, route, params)

      {
        :ok,
        rushing_exports: rushing_exports, response_conn: conn
      }
    end

    @tag params: %{}
    test "success: lists first 10 (default) rushing_exports",
         %{response_conn: conn, rushing_exports: _rushing_exports} do
      # ok response
      assert conn.status == 200

      %{"data" => results} = json_response(conn, 200)
      assert length(results) == 10
    end
  end

  describe "POST /api/rushing_exports" do
    setup %{conn: conn} = context do
      route = Routes.rushing_export_path(conn, :create)

      params = context[:params]

      conn = post(conn, route, params)

      {
        :ok,
        response_conn: conn
      }
    end

    @tag params: Factory.params_for(:rushing_export)
    test "success: inserts a rushing export with state new",
         %{response_conn: conn} do
      # ok response
      assert conn.status == 200

      %{"data" => result} = json_response(conn, 200)

      assert "state" in Map.keys(result)
      assert "updated_at" in Map.keys(result)
      assert "inserted_at" in Map.keys(result)
      assert result["state"] == "new"

      id = result["id"]

      # cooldown (enough time to process)
      :timer.sleep(1_000)
      updated_rushing_export = TheRush.Repo.get(TheRush.RushingExport, id)

      assert updated_rushing_export.state == :completed
    end

    @tag params: %{params: %{sort_by: :bad}}
    test "failure: doesn't insert export if bad params",
         %{response_conn: conn} do
      assert conn.status == 422
      %{"errors" => errors} = json_response(conn, 422)
      assert errors == [%{"field" => "params.sort_by", "message" => "is invalid"}]
    end
  end

  describe "GET /api/rushing_exports/:id" do
    setup %{conn: conn} = context do
      rushing_export = Factory.insert(:rushing_export)

      id = context[:id] || rushing_export.id

      route = Routes.rushing_export_path(conn, :show, id)

      conn = get(conn, route, %{})

      {
        :ok,
        response_conn: conn, rushing_export: rushing_export
      }
    end

    test "success: when params has the correct id", %{response_conn: conn} do
      assert conn.status == 200

      %{"data" => result} = json_response(conn, 200)

      assert "state" in Map.keys(result)
      assert "updated_at" in Map.keys(result)
      assert "inserted_at" in Map.keys(result)
      assert result["state"] == "new"
    end

    @tag id: -1
    test "failure: when params have incorrect id", %{response_conn: conn} do
      assert conn.status == 404

      %{"errors" => errors} = json_response(conn, 404)

      assert errors == ["Not found"]
    end
  end
end

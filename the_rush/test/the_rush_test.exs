defmodule TheRushTest do
  use TheRush.DataCase

  alias TheRush.Factory
  alias TheRush.Rushing

  describe "import_from_json/1" do
    setup tags do
      json = tags[:json]
      result = TheRush.import_from_json(json)
      {:ok, result: result}
    end

    @valid_json "./test/support/example_rushings.json"
    @tag json: @valid_json
    test "success: returns {:ok, rushings} and inserts rushings when valid",
         %{result: result} do
      {:ok, rushings} = result

      assert length(rushings) == 2

      [rushing1, rushing2] = rushings

      expected_rushing1_params = %{
        player: "Joe Banyard",
        team: "JAX",
        position: "RB",
        attempts: 2,
        attempts_per_game: 2.0,
        yards: 7,
        average: 3.5,
        yards_per_game: 7.0,
        touchdowns: 0,
        longest_rush: 7,
        longest_rush_touchdown: false,
        first_downs: 0,
        first_downs_percentage: 0.0,
        yards_20_plus: 0,
        yards_40_plus: 0,
        fumbles: 0
      }

      expected_rushing2_params = %{
        player: "Shaun Hill",
        team: "MIN",
        position: "QB",
        attempts: 5,
        attempts_per_game: 1.7,
        yards: 5,
        average: 1.0,
        yards_per_game: 1.7,
        touchdowns: 0,
        longest_rush: 9,
        longest_rush_touchdown: true,
        first_downs: 0,
        first_downs_percentage: 0.0,
        yards_20_plus: 0,
        yards_40_plus: 0,
        fumbles: 0
      }

      refute is_nil(rushing1.id)

      for field <- Rushing.__schema__(:fields) -- [:id] do
        expected = expected_rushing1_params[field]
        actual = Map.get(rushing1, field)

        assert(
          values_equal?(actual, expected),
          "Expected value for #{field} to be #{expected}, but got #{actual}"
        )
      end

      refute is_nil(rushing2.id)

      for field <- Rushing.__schema__(:fields) -- [:id] do
        expected = expected_rushing2_params[field]
        actual = Map.get(rushing2, field)

        assert(
          values_equal?(actual, expected),
          "Expected value for #{field} to be #{expected}, but got #{actual}"
        )
      end
    end

    @invalid_json "./test/support/invalid_rushings.json"
    @tag json: @invalid_json
    test "failure: returns {:error, player error} and doesn't insert rushings",
         %{result: result} do
      {:error, errors} = result
      assert errors == [%{field: "player", message: "can't be blank"}]
    end

    @invalid_file "./test/support/bad/file/path.json"
    @tag json: @invalid_file
    test "failure: returns {:error, file_error} and doesn't insert rushings",
         %{result: result} do
      {:error, reason} = result
      assert reason == :enoent
    end
  end

  describe "list_rushings/1" do
    setup do
      rushings = Enum.map(1..5, fn _ -> Factory.insert(:rushing) end)
      {:ok, rushings: rushings}
    end

    test "success: when valid params, with sort & page yields expected results",
         %{rushings: rushings} do
      params = Factory.params_for(:list_rushings_params, %{sort_by: :yards})

      {
        :ok,
        %{
          rushings: results,
          page_info: page_info
        }
      } = TheRush.list_rushings(params)

      # Check length
      assert length(rushings) == length(results)

      # expect results are sorted by ascending yards
      assert Enum.sort_by(rushings, &Map.get(&1, :yards)) == results

      assert page_info == %{
               page: 1,
               page_size: 10,
               total_pages: 1
             }

      params =
        Factory.params_for(
          :list_rushings_params,
          %{sort_by: :yards, sort_dir: :desc, page_size: 3}
        )

      {
        :ok,
        %{
          rushings: results,
          page_info: page_info
        }
      } = TheRush.list_rushings(params)

      # Check it only lists 3
      assert length(rushings) != length(results)
      assert length(results) == 3

      # expect results are sorted by descending yards
      sorted_rushings = Enum.sort_by(rushings, &Map.get(&1, :yards), :desc)

      # Only lists first three of sorted rushings
      assert Enum.take(sorted_rushings, 3) == results

      assert page_info == %{
               page: 1,
               page_size: 3,
               total_pages: ceil(5 / 3)
             }

      params =
        Factory.params_for(
          :list_rushings_params,
          %{sort_by: :yards, sort_dir: :desc, page_size: 2, page: 3}
        )

      {
        :ok,
        %{
          rushings: results,
          page_info: page_info
        }
      } = TheRush.list_rushings(params)

      # Check it only lists 1. The last page has only one result even though the
      # page size is 2
      assert length(rushings) != length(results)
      assert length(results) == 1

      # expect results are sorted by descending yards
      sorted_rushings = Enum.sort_by(rushings, &Map.get(&1, :yards), :desc)
      # Only lists the last element of sorted rushings
      [result] = results
      assert Enum.at(sorted_rushings, -1) == result

      assert page_info == %{
               page: 3,
               page_size: 2,
               total_pages: ceil(5 / 2)
             }
    end

    test "success: when valid params, with player, yields expected results",
         %{rushings: rushings} do
      [head | _tail] = rushings

      params =
        Factory.params_for(
          :list_rushings_params,
          %{player: head.player}
        )

      {
        :ok,
        %{
          rushings: results,
          page_info: page_info
        }
      } = TheRush.list_rushings(params)

      # Only lists the first rushing
      assert length(results) == 1
      [result] = results
      assert head == result

      assert page_info == %{
               page: 1,
               page_size: 10,
               total_pages: 1
             }
    end

    test "failure: when params are invalid" do
      params =
        Factory.params_for(
          :list_rushings_params,
          %{sort_by: :bad_value}
        )

      {:error, reason} = TheRush.list_rushings(params)
      assert reason == [%{field: "sort_by", message: "is invalid"}]
    end
  end

  require Decimal

  defp values_equal?(actual, expected) do
    cond do
      Decimal.is_decimal(actual) and is_float(expected) ->
        Decimal.eq?(actual, Decimal.from_float(expected))

      Decimal.is_decimal(expected) and is_float(actual) ->
        Decimal.eq?(expected, Decimal.from_float(actual))

      true ->
        actual == expected
    end
  end
end

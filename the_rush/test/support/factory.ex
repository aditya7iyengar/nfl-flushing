defmodule TheRush.Factory do
  @moduledoc """
  Factory for data in the_rush backend app
  """

  use ExMachina.Ecto, repo: TheRush.Repo

  @example_positions ~w[RB QB WR FB P NT TE DB]

  def rushing_factory do
    %TheRush.Rushing{
      player: sequence(:rushing_player, fn index -> "Player #{index}" end),
      team: sequence(:rushing_team, fn index -> "Team #{index}" end),
      position: Enum.random(@example_positions),
      attempts: Enum.random(0..150),
      # random decimal between 0.00 - 50.00 with a precision of 2
      attempts_per_game: Enum.random(0..5000) / 100,
      yards: Enum.random(0..400),
      average: Enum.random(-200..200) / 10,
      yards_per_game: Enum.random(-300..300) / 10,
      touchdowns: Enum.random(0..15),
      longest_rush: Enum.random(0..90),
      longest_rush_touchdown: Enum.random([true, false]),
      first_downs: Enum.random(0..80),
      first_downs_percentage: Enum.random(0..1000) / 10,
      yards_20_plus: Enum.random(0..20),
      yards_40_plus: Enum.random(0..10),
      fumbles: Enum.random(0..15)
    }
  end

  def list_rushings_params_factory do
    %TheRush.ListRushingsParams{page: 1, page_size: 10}
  end

  def rushing_export_factory do
    %TheRush.RushingExport{
      params: build(:list_rushings_params),
      state: :new
    }
  end

  def list_rushing_exports_params_factory do
    %TheRush.ListRushingExportsParams{page: 1, page_size: 10}
  end
end

defmodule TheRush.ExportWorkerTest do
  use TheRush.DataCase, async: false

  alias TheRush.ExportProcessor
  alias TheRush.ExportWorker
  alias TheRush.Factory

  describe "async process" do
    setup do
      dir =
        Application.app_dir(
          :the_rush,
          "rushing_export"
        )

      on_exit(fn -> File.rm_rf!(dir) end)

      :ok
    end

    test "success: successfully processes an export" do
      rushing = Factory.insert(:rushing)
      rushing_export = Factory.insert(:rushing_export)
      path = ExportProcessor.csv_file(rushing_export)
      refute File.exists?(path)
      # enqueue
      GenServer.cast(ExportWorker, {:enqueue, rushing_export})

      # cooldown (enough time to process)
      :timer.sleep(1_000)

      assert File.exists?(path)
      {:ok, file} = File.open(path, [:read])

      headers = IO.read(file, :line)
      expected_fields = TheRush.Rushing.__schema__(:fields) -- [:id]

      expected_headers =
        expected_fields
        |> Enum.map(&to_string/1)
        |> Enum.join(",")

      assert headers == expected_headers <> "\n"
      line1 = IO.read(file, :line)

      expected_line =
        expected_fields
        |> Enum.map(&Map.get(rushing, &1))
        |> Enum.join(",")

      assert line1 == expected_line <> "\n"

      next = IO.read(file, :line)
      ## End of file
      assert next == :eof
    end
  end
end

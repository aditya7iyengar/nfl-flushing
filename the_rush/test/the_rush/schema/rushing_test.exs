defmodule TheRush.RushingTest do
  use TheRush.DataCase

  alias TheRush.Factory
  alias TheRush.Rushing

  describe "schema" do
    test "schema source is rushings table with primary key set id" do
      assert Rushing.__schema__(:source) == "rushings"
      assert Rushing.__schema__(:primary_key) == [:id]
    end

    test "fields and their types are set as expected" do
      expected_fields = %{
        id: :id,
        player: :string,
        team: :string,
        position: :string,
        attempts: :integer,
        attempts_per_game: :decimal,
        yards: :integer,
        average: :decimal,
        yards_per_game: :decimal,
        touchdowns: :integer,
        longest_rush: :integer,
        longest_rush_touchdown: :boolean,
        first_downs: :integer,
        first_downs_percentage: :decimal,
        yards_20_plus: :integer,
        yards_40_plus: :integer,
        fumbles: :integer
      }

      actual_fields = Rushing.__changeset__()

      for field <- Rushing.__schema__(:fields) do
        expected = expected_fields[field]
        actual = actual_fields[field]

        assert(
          actual == expected,
          "Type for #{field} was expected to be #{expected} but got #{actual}"
        )
      end
    end
  end

  describe "changeset/1" do
    setup tags do
      params = tags[:params]
      result = Rushing.changeset(params)
      {:ok, result: result}
    end

    @valid_params Factory.params_for(:rushing)
    @tag params: @valid_params
    test "success: returns a valid changeset when valid params are given",
         %{result: changeset} do
      assert changeset.valid? == true
      struct = Ecto.Changeset.apply_changes(changeset)

      for key <- Map.keys(@valid_params) do
        expected = @valid_params[key]
        actual = Map.get(struct, key)

        assert(
          values_equal?(actual, expected),
          "Expected value for #{key} to be #{expected} but got #{actual}"
        )
      end
    end

    @expected_required_fields Rushing.__schema__(:fields) -- [:id]
    @empty_params %{}
    @tag params: @empty_params
    test "failure: returns an invalid changeset, with proper errors / defaults",
         %{result: changeset} do
      assert changeset.valid? == false

      for field <- @expected_required_fields do
        assert(
          field in changeset.required,
          "Expected #{field} to be in required, but it was not"
        )
      end

      struct_with_defaults = Ecto.Changeset.apply_changes(changeset)

      defaults = %{longest_rush_touchdown: false}

      for key <- Map.keys(defaults) do
        expected = defaults[key]
        actual = Map.get(struct_with_defaults, key)

        assert(
          values_equal?(actual, expected),
          "Expected default for #{key} to be #{expected} but got #{actual}"
        )
      end
    end

    @gt_pct_params Factory.params_for(:rushing, first_downs_percentage: 101.0)
    @tag params: @gt_pct_params
    test "failure: returns an invalid changeset with expected error when " <>
           "percentage is greater than 100",
         %{result: changeset} do
      assert changeset.valid? == false

      assert changeset.errors == [
               {:first_downs_percentage, {"cannot be greater than 100.0", []}}
             ]
    end

    @lt_pct_params Factory.params_for(:rushing, first_downs_percentage: -1.0)
    @tag params: @lt_pct_params
    test "failure: returns an invalid changeset with expected error when " <>
           "percentage is less than 0.0",
         %{result: changeset} do
      assert changeset.valid? == false

      assert changeset.errors == [
               {:first_downs_percentage, {"cannot be less than 0.0", []}}
             ]
    end
  end

  require Decimal

  defp values_equal?(actual, expected) do
    cond do
      Decimal.is_decimal(actual) and is_float(expected) ->
        Decimal.eq?(actual, Decimal.from_float(expected))

      Decimal.is_decimal(expected) and is_float(actual) ->
        Decimal.eq?(expected, Decimal.from_float(actual))

      true ->
        actual == expected
    end
  end
end

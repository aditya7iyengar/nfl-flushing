defmodule TheRush.ListRushingsParamsTest do
  use TheRush.DataCase

  alias TheRush.Factory
  alias TheRush.ListRushingsParams

  describe "schema" do
    test "embedded schema without primary key" do
      assert is_nil(ListRushingsParams.__schema__(:source))
      assert ListRushingsParams.__schema__(:primary_key) == []
    end

    test "fields and their types are set as expected" do
      sort_by_type = {
        :parameterized,
        Ecto.Enum,
        %{
          mappings: [
            yards: "yards",
            longest_rush: "longest_rush",
            touchdowns: "touchdowns"
          ],
          on_cast: %{
            "longest_rush" => :longest_rush,
            "touchdowns" => :touchdowns,
            "yards" => :yards
          },
          on_dump: %{
            longest_rush: "longest_rush",
            touchdowns: "touchdowns",
            yards: "yards"
          },
          on_load: %{
            "longest_rush" => :longest_rush,
            "touchdowns" => :touchdowns,
            "yards" => :yards
          },
          type: :string
        }
      }

      sort_dir_type = {
        :parameterized,
        Ecto.Enum,
        %{
          mappings: [asc: "asc", desc: "desc"],
          on_cast: %{"asc" => :asc, "desc" => :desc},
          on_dump: %{asc: "asc", desc: "desc"},
          on_load: %{"asc" => :asc, "desc" => :desc},
          type: :string
        }
      }

      expected_fields = %{
        page: :integer,
        page_size: :integer,
        player: :string,
        sort_by: sort_by_type,
        sort_dir: sort_dir_type
      }

      actual_fields = ListRushingsParams.__changeset__()

      for field <- ListRushingsParams.__schema__(:fields) do
        expected = expected_fields[field]
        actual = actual_fields[field]

        assert(
          actual == expected,
          "Type for #{field} was expected to be #{inspect(expected)} but got" <>
            " #{inspect(actual)}"
        )
      end
    end
  end

  describe "changeset/1" do
    setup tags do
      params = tags[:params]
      result = ListRushingsParams.changeset(params)
      {:ok, result: result}
    end

    @valid_params Factory.params_for(:list_rushings_params)
    @tag params: @valid_params
    test "success: returns a valid changeset when valid params are given",
         %{result: changeset} do
      assert changeset.valid? == true
      struct = Ecto.Changeset.apply_changes(changeset)

      for key <- Map.keys(@valid_params) do
        expected = @valid_params[key]
        actual = Map.get(struct, key)

        assert(
          values_equal?(actual, expected),
          "Expected value for #{key} to be #{expected} but got #{actual}"
        )
      end
    end

    @expected_required_fields ~w[page page_size]a
    @empty_params %{}
    @tag params: @empty_params
    test "failure: returns a valid changeset, with proper errors / defaults",
         %{result: changeset} do
      assert changeset.valid? == true

      for field <- @expected_required_fields do
        assert(
          field in changeset.required,
          "Expected #{field} to be in required, but it was not"
        )
      end

      struct_with_defaults = Ecto.Changeset.apply_changes(changeset)

      defaults = %{page: 1, page_size: 10, sort_dir: :asc}

      for key <- Map.keys(defaults) do
        expected = defaults[key]
        actual = Map.get(struct_with_defaults, key)

        assert(
          values_equal?(actual, expected),
          "Expected default for #{key} to be #{expected} but got #{actual}"
        )
      end
    end
  end

  require Decimal

  defp values_equal?(actual, expected) do
    cond do
      Decimal.is_decimal(actual) and is_float(expected) ->
        Decimal.eq?(actual, Decimal.from_float(expected))

      Decimal.is_decimal(expected) and is_float(actual) ->
        Decimal.eq?(expected, Decimal.from_float(actual))

      true ->
        actual == expected
    end
  end
end

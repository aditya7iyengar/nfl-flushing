defmodule TheRush.ListRushingExportsParamsTest do
  use TheRush.DataCase

  alias TheRush.Factory
  alias TheRush.ListRushingExportsParams

  describe "schema" do
    test "embedded schema without primary key" do
      assert is_nil(ListRushingExportsParams.__schema__(:source))
      assert ListRushingExportsParams.__schema__(:primary_key) == []
    end

    test "fields and their types are set as expected" do
      expected_fields = %{
        page: :integer,
        page_size: :integer
      }

      actual_fields = ListRushingExportsParams.__changeset__()

      for field <- ListRushingExportsParams.__schema__(:fields) do
        expected = expected_fields[field]
        actual = actual_fields[field]

        assert(
          actual == expected,
          "Type for #{field} was expected to be #{inspect(expected)} but got" <>
            " #{inspect(actual)}"
        )
      end
    end
  end

  describe "changeset/1" do
    setup tags do
      params = tags[:params]
      result = ListRushingExportsParams.changeset(params)
      {:ok, result: result}
    end

    @valid_params Factory.params_for(:list_rushing_exports_params)
    @tag params: @valid_params
    test "success: returns a valid changeset when valid params are given",
         %{result: changeset} do
      assert changeset.valid? == true
      struct = Ecto.Changeset.apply_changes(changeset)

      for key <- Map.keys(@valid_params) do
        expected = @valid_params[key]
        actual = Map.get(struct, key)

        assert(
          values_equal?(actual, expected),
          "Expected value for #{key} to be #{expected} but got #{actual}"
        )
      end
    end

    @expected_required_fields ~w[page page_size]a
    @empty_params %{}
    @tag params: @empty_params
    test "failure: returns a valid changeset, with proper errors / defaults",
         %{result: changeset} do
      assert changeset.valid? == true

      for field <- @expected_required_fields do
        assert(
          field in changeset.required,
          "Expected #{field} to be in required, but it was not"
        )
      end

      struct_with_defaults = Ecto.Changeset.apply_changes(changeset)

      defaults = %{page: 1, page_size: 10}

      for key <- Map.keys(defaults) do
        expected = defaults[key]
        actual = Map.get(struct_with_defaults, key)

        assert(
          values_equal?(actual, expected),
          "Expected default for #{key} to be #{expected} but got #{actual}"
        )
      end
    end
  end

  require Decimal

  defp values_equal?(actual, expected) do
    cond do
      Decimal.is_decimal(actual) and is_float(expected) ->
        Decimal.eq?(actual, Decimal.from_float(expected))

      Decimal.is_decimal(expected) and is_float(actual) ->
        Decimal.eq?(expected, Decimal.from_float(actual))

      true ->
        actual == expected
    end
  end
end

defmodule TheRush.RushingExportTest do
  use TheRush.DataCase

  alias TheRush.Factory
  alias TheRush.RushingExport

  describe "schema" do
    test "schema source is rushing_exports table with primary key set id" do
      assert RushingExport.__schema__(:source) == "rushing_exports"
      assert RushingExport.__schema__(:primary_key) == [:id]
    end

    test "fields and their types are set as expected" do
      state_type = {
        :parameterized,
        Ecto.Enum,
        %{
          mappings: [
            new: "new",
            processing: "processing",
            completed: "completed",
            failed: "failed"
          ],
          on_cast: %{
            "completed" => :completed,
            "failed" => :failed,
            "new" => :new,
            "processing" => :processing
          },
          on_dump: %{
            completed: "completed",
            failed: "failed",
            new: "new",
            processing: "processing"
          },
          on_load: %{
            "completed" => :completed,
            "failed" => :failed,
            "new" => :new,
            "processing" => :processing
          },
          type: :string
        }
      }

      params_type = {
        :embed,
        %Ecto.Embedded{
          cardinality: :one,
          field: :params,
          on_cast: nil,
          on_replace: :raise,
          ordered: true,
          owner: TheRush.RushingExport,
          related: TheRush.ListRushingsParams,
          unique: true
        }
      }

      expected_fields = %{
        id: :id,
        state: state_type,
        params: params_type,
        inserted_at: :naive_datetime,
        updated_at: :naive_datetime
      }

      actual_fields = RushingExport.__changeset__()

      for field <- RushingExport.__schema__(:fields) do
        expected = expected_fields[field]
        actual = actual_fields[field]

        assert(
          actual == expected,
          "Type for #{field} was expected to be #{inspect(expected)} but got" <>
            " #{inspect(actual)}"
        )
      end
    end
  end

  describe "changeset/1" do
    setup tags do
      params = tags[:params]

      result = RushingExport.changeset(params)
      {:ok, result: result}
    end

    @valid_params Factory.params_for(:rushing_export)
    @tag params: @valid_params
    test "success: returns a valid changeset when valid params are given",
         %{result: changeset} do
      assert changeset.valid? == true
      struct = Ecto.Changeset.apply_changes(changeset)

      assert struct.state == Map.get(@valid_params, :state)

      for key <- Map.keys(@valid_params[:params]) do
        expected = Map.get(@valid_params[:params], key)
        actual = Map.get(struct.params, key)

        assert(
          values_equal?(actual, expected),
          "Expected default for #{key} to be #{expected} but got #{actual}"
        )
      end
    end

    @expected_required_fields []
    @empty_params %{}
    @tag params: @empty_params
    test "failure: returns a valid changeset, with proper defaults",
         %{result: changeset} do
      assert changeset.valid? == true

      for field <- @expected_required_fields do
        assert(
          field in changeset.required,
          "Expected #{field} to be in required, but it was not"
        )
      end

      struct_with_defaults = Ecto.Changeset.apply_changes(changeset)

      defaults = %{state: :new}

      for key <- Map.keys(defaults) do
        expected = defaults[key]
        actual = Map.get(struct_with_defaults, key)

        assert(
          values_equal?(actual, expected),
          "Expected default for #{key} to be #{expected} but got #{actual}"
        )
      end
    end
  end

  require Decimal

  defp values_equal?(actual, expected) do
    cond do
      Decimal.is_decimal(actual) and is_float(expected) ->
        Decimal.eq?(actual, Decimal.from_float(expected))

      Decimal.is_decimal(expected) and is_float(actual) ->
        Decimal.eq?(expected, Decimal.from_float(actual))

      true ->
        actual == expected
    end
  end
end

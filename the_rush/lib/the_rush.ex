defmodule TheRush do
  @moduledoc """
  Context to interact with TheRush and DB
  """

  @json_to_field_mapping %{
    "Player" => :player,
    "Team" => :team,
    "Pos" => :position,
    "Att" => :attempts,
    "Att/G" => :attempts_per_game,
    "Yds" => :yards,
    "Avg" => :average,
    "Yds/G" => :yards_per_game,
    "TD" => :touchdowns,
    "Lng" => :longest_rush,
    "1st" => :first_downs,
    "1st%" => :first_downs_percentage,
    "20+" => :yards_20_plus,
    "40+" => :yards_40_plus,
    "FUM" => :fumbles
  }

  # Using schema reflection to get number fields:
  # This is to get all the integer and decimal fields.
  # This will further be used for formatting purposes like removing commas.
  @number_types ~w[integer decimal]a
  @number_fields :fields
                 |> TheRush.Rushing.__schema__()
                 |> Enum.filter(fn field ->
                   TheRush.Rushing.__schema__(:type, field) in @number_types
                 end)
  @doc """
  Imports Rushing(s) from a json file

  Expected format:
  ```json
  [
    {
      "Player":"Joe Banyard",
      "Team":"JAX",
      "Pos":"RB",
      "Att":2,
      "Att/G":2,
      "Yds":7,
      "Avg":3.5,
      "Yds/G":7,
      "TD":0,
      "Lng":"7",
      "1st":0,
      "1st%":0,
      "20+":0,
      "40+":0,
      "FUM":0
    },
    ..
  ]
  ```
  """
  def import_from_json(file_path) do
    with {:ok, file_contents} <- File.read(file_path),
         {:ok, decoded_json} <- Jason.decode(file_contents),
         rushings_params = json_to_rushings_params(decoded_json),
         {:ok, rushings} <- insert_rushings(rushings_params) do
      {:ok, Map.values(rushings)}
    else
      {:error, reason} ->
        {:error, reason}

      {:error, {:rushing, _index}, changeset, _} ->
        {:error, errors_from_changeset(changeset)}
    end
  end

  # Translates the nested maps and lists output of changeset errors
  defp errors_from_changeset(changeset) do
    changeset
    |> Ecto.Changeset.traverse_errors(&translate_changeset_error/1)
    |> recursive_changeset_error_list()
  end

  defp recursive_changeset_error_list(errors) do
    Enum.reduce(errors, [], fn
      {field, errors}, acc when is_list(errors) ->
        Enum.map(
          errors,
          fn error ->
            %{field: to_string(field), message: error}
          end
        ) ++ acc

      {field, nested_errors}, acc when is_map(errors) ->
        recursive_changeset_error_list(
          Enum.into(
            nested_errors,
            %{},
            fn {nested_field, errors} ->
              {"#{field}.#{nested_field}", errors}
            end
          )
        ) ++ acc
    end)
  end

  defp translate_changeset_error({msg, opts}) do
    Enum.reduce(opts, msg, fn {key, value}, acc ->
      String.replace(acc, "%{#{key}}", fn _ -> to_string(value) end)
    end)
  end

  defp json_to_rushings_params(decoded_json) when is_list(decoded_json) do
    Enum.map(decoded_json, &json_to_rushing_params/1)
  end

  defp json_to_rushings_params(_), do: {:error, :bad_json}

  defp json_to_rushing_params(json_params) do
    for {key, value} <- json_params, into: %{} do
      field = @json_to_field_mapping[key]
      formatted_value = format_field_value(field, value)
      {field, formatted_value}
    end
  end

  defp format_field_value(field, value) when field in @number_fields do
    format_number_value(value)
  end

  # TODO: propagate this using {:error, type}
  defp format_field_value(nil, _value), do: raise("unexpected error")

  defp format_field_value(_field, value), do: value

  defp format_number_value(value) when is_binary(value) do
    # Remove commas
    String.replace(value, ",", "")
  end

  defp format_number_value(value), do: value

  alias Ecto.Multi
  alias TheRush.{Repo, Rushing}

  defp insert_rushings(rushings_params) do
    rushings_params
    |> Enum.map(&cast_longest_rush/1)
    |> Enum.reduce({0, Multi.new()}, fn params, {index, multi} ->
      multi =
        Multi.insert(
          multi,
          {:rushing, index},
          Rushing.changeset(params)
        )

      {index + 1, multi}
    end)
    |> elem(1)
    |> Repo.transaction()
  end

  defp cast_longest_rush(params) do
    lr = params[:longest_rush]

    cond do
      is_nil(lr) ->
        params

      is_integer(lr) ->
        params

      lr =~ "T" ->
        %{"num" => longest_rush} = Regex.named_captures(~r/(?<num>.*)T$/, lr)

        params
        |> Map.put(:longest_rush, longest_rush)
        |> Map.put(:longest_rush_touchdown, true)

      true ->
        params
    end
  end

  alias Ecto.Changeset
  alias TheRush.ListRushingsParams

  @doc """
  List Rushings with parameters

  * page: page number
  * page_size: size of the page
  * sort_by (optional): ["yards", "longest_rush", "touchdowns"]
  * sort_dir (default asc): ["asc", "desc"]
  """
  def list_rushings(params) do
    changeset = ListRushingsParams.changeset(params)

    if changeset.valid? == true do
      params = Changeset.apply_changes(changeset)

      rushings = do_list_rushings(params)
      page_info = get_page_info(params)

      {:ok, %{rushings: rushings, page_info: page_info}}
    else
      {:error, errors_from_changeset(changeset)}
    end
  end

  import Ecto.Query

  def do_list_rushings(%{page_size: page_size} = params) do
    offset = (params.page - 1) * (page_size || 0)

    Rushing
    |> maybe_add_player_filter(params)
    |> maybe_add_list_sort(params)
    |> maybe_add_limit(page_size)
    |> offset(^offset)
    |> Repo.all()
  end

  defp get_page_info(%{page_size: page_size, page: page} = params) do
    total_pages = params |> get_page_count() |> (&(&1 / page_size)).() |> ceil()

    %{
      page_size: page_size,
      page: page,
      total_pages: total_pages
    }
  end

  defp get_page_count(params) do
    Rushing
    |> maybe_add_player_filter(params)
    |> add_count()
    |> Repo.one()
  end

  defp maybe_add_limit(query, nil), do: query

  defp maybe_add_limit(query, page_size), do: limit(query, ^page_size)

  defp maybe_add_player_filter(query, %{player: nil}), do: query

  # case-insensitive matching
  defp maybe_add_player_filter(query, %{player: player}) do
    player_query = "%#{player}%"
    where(query, [r], ilike(r.player, ^player_query))
  end

  defp maybe_add_list_sort(query, %{sort_by: nil}), do: query

  defp maybe_add_list_sort(query, %{sort_by: sort_by} = params) do
    sort_dir = params.sort_dir
    order_by(query, {^sort_dir, ^sort_by})
  end

  defp add_count(query), do: select(query, [p], count(p.id))

  alias TheRush.{ListRushingExportsParams, RushingExport}

  @doc """
  List all RushingExports
  """
  def list_rushing_exports(params) do
    changeset = ListRushingExportsParams.changeset(params)

    if changeset.valid? == true do
      results =
        changeset
        |> Changeset.apply_changes()
        |> do_list_rushing_exports()

      {:ok, results}
    else
      {:error, errors_from_changeset(changeset)}
    end
  end

  defp do_list_rushing_exports(params) do
    offset = (params.page - 1) * params.page_size

    RushingExport
    |> add_default_exports_sort()
    |> limit(^params.page_size)
    |> offset(^offset)
    |> Repo.all()
  end

  defp add_default_exports_sort(query) do
    order_by(query, desc: :updated_at)
  end

  @doc """
  Create RushingExport
  """
  def create_rushing_export(params) do
    result =
      %{params: params}
      |> RushingExport.changeset()
      |> Repo.insert()

    case result do
      {:ok, rushing_export} ->
        TheRush.ExportWorker.enqueue(rushing_export)
        {:ok, rushing_export}

      {:error, changeset} ->
        {:error, errors_from_changeset(changeset)}
    end
  end

  @doc """
  Fetch a RushingExport
  """
  def get_rushing_export(id) do
    case Repo.get(RushingExport, id) do
      nil -> {:error, :not_found}
      rushing_export -> {:ok, rushing_export}
    end
  end

  @doc """
  Update RushingExport state
  """
  def update_rushing_export_state(rushing_export, state) do
    rushing_export
    |> RushingExport.state_changeset(%{state: state})
    |> Repo.update()
  end
end

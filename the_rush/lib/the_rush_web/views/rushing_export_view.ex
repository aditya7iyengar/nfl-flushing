defmodule TheRushWeb.RushingExportView do
  use TheRushWeb, :view

  def render("index.json", %{rushing_exports: rushing_exports}) do
    %{data: Enum.map(rushing_exports, &rushing_export_data/1)}
  end

  def render("show.json", %{rushing_export: rushing_export}) do
    %{data: rushing_export_data(rushing_export)}
  end

  @rushing_export_data_fields ~w[id state updated_at inserted_at]a

  defp rushing_export_data(rushing_export) do
    Map.take(rushing_export, @rushing_export_data_fields)
  end
end

defmodule TheRushWeb.RushingView do
  use TheRushWeb, :view

  def render("index.json", %{rushings: rushings, page_info: page_info}) do
    %{
      data: Enum.map(rushings, &rushing_data/1),
      page_info: render_page_info(page_info)
    }
  end

  @rushing_data_fields TheRush.Rushing.__schema__(:fields) -- [:id]

  defp rushing_data(rushing) do
    Map.take(rushing, @rushing_data_fields)
  end

  defp render_page_info(page_info) do
    %{
      page: page_info[:page],
      page_size: page_info[:page_size],
      total_pages: page_info[:total_pages]
    }
  end
end

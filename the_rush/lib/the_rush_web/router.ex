defmodule TheRushWeb.Router do
  use TheRushWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", TheRushWeb do
    pipe_through :api

    get "/rushings", RushingController, :index

    get "/rushing_exports/:id/download_csv", RushingExportController, :download_csv
    post "/rushing_exports/sync_create", RushingExportController, :sync_create

    resources "/rushing_exports",
              RushingExportController,
              only: ~w[create show index]a

  end
end

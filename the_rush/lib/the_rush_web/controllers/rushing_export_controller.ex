defmodule TheRushWeb.RushingExportController do
  use TheRushWeb, :controller

  def sync_create(conn, params) do
    with {:ok, rushing_export} <- TheRush.create_rushing_export(params),
         {:ok, rushing_export} <- check_processed(rushing_export) do
      render(conn, "show.json", rushing_export: rushing_export)
    else
      {:error, _} ->
        conn
        |> put_status(422)
        |> put_view(TheRushWeb.ErrorView)
        |> render("error.json", error: "Something went wrong")
    end
  end

  @processed_states ~w[completed failed]a
  @sync_sleep_time_ms 50

  defp check_processed(rushing_export) do
    with {:ok, rushing_export} <- TheRush.get_rushing_export(rushing_export.id),
         {:state, true} <- {:state, rushing_export.state in @processed_states} do
      {:ok, rushing_export}
    else
      {:error, error} ->
        {:error, error}

      {:state, false} ->
        :timer.sleep(@sync_sleep_time_ms)
        check_processed(rushing_export)
    end
  end

  def create(conn, params) do
    case TheRush.create_rushing_export(params) do
      {:ok, rushing_export} ->
        render(conn, "show.json", rushing_export: rushing_export)

      {:error, error} ->
        conn
        |> put_status(422)
        |> put_view(TheRushWeb.ErrorView)
        |> render("error.json", error: error)
    end
  end

  def show(conn, params) do
    case TheRush.get_rushing_export(params["id"]) do
      {:ok, rushing_export} ->
        render(conn, "show.json", rushing_export: rushing_export)

      {:error, :not_found} ->
        conn
        |> put_status(404)
        |> put_view(TheRushWeb.ErrorView)
        |> render("error.json", error: "Not found")
    end
  end

  def index(conn, params) do
    case TheRush.list_rushing_exports(params) do
      {:ok, rushing_exports} ->
        render(conn, "index.json", rushing_exports: rushing_exports)

      {:error, error} ->
        conn
        |> put_status(422)
        |> put_view(TheRushWeb.ErrorView)
        |> render("error.json", error: error)
    end
  end

  def download_csv(conn, params) do
    with {:ok, rushing_export} <- TheRush.get_rushing_export(params["id"]),
         {:state, true} <- {:state, rushing_export.state == :completed} do
      csv_path = TheRush.ExportProcessor.csv_file(rushing_export)
      send_download(conn, {:file, csv_path})
    else
      {:state, false} ->
        conn
        |> put_status(403)
        |> put_view(TheRushWeb.ErrorView)
        |> render("error.json", error: "Csv not ready for download")

      {:error, :not_found} ->
        conn
        |> put_status(404)
        |> put_view(TheRushWeb.ErrorView)
        |> render("error.json", error: "Not found")
    end
  end
end

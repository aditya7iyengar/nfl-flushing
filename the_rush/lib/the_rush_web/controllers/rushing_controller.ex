defmodule TheRushWeb.RushingController do
  use TheRushWeb, :controller

  def index(conn, params) do
    case TheRush.list_rushings(params) do
      {:ok, %{rushings: rushings, page_info: page_info}} ->
        render(conn, "index.json", rushings: rushings, page_info: page_info)

      {:error, error} ->
        conn
        |> put_status(422)
        |> put_view(TheRushWeb.ErrorView)
        |> render("error.json", error: error)
    end
  end
end

defmodule TheRush.ExportWorker do
  use GenServer

  ## Interface (Client)

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  @cooldown_ms 1_000

  def work do
    state = GenServer.call(__MODULE__, :get)

    if length(state) > 0 do
      _export = GenServer.call(__MODULE__, :process)
      work()
    else
      # cooldown if there's nothing in the queue
      :timer.sleep(@cooldown_ms)
      work()
    end
  end

  def enqueue(export), do: GenServer.cast(__MODULE__, {:enqueue, export})

  ## Callbacks (Server)

  @impl true
  def init(queue), do: {:ok, queue}

  @impl true
  def handle_cast({:enqueue, export}, queue), do: {:noreply, queue ++ [export]}

  @impl true
  def handle_call(:process, _from, [export | tail]) do
    TheRush.ExportProcessor.process_export(export)
    {:reply, export, tail}
  end

  @impl true
  def handle_call(:get, _from, state), do: {:reply, state, state}
end

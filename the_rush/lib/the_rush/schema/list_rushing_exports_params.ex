defmodule TheRush.ListRushingExportsParams do
  @moduledoc """
  Validator for list_rushing_exports call
  """
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  embedded_schema do
    field(:page, :integer, default: 1)
    field(:page_size, :integer, default: 10)
  end

  @required_fields ~w[page page_size]a

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, __schema__(:fields))
    |> validate_required(@required_fields)
  end
end

defmodule TheRush.Rushing do
  @moduledoc """
  Ecto schema representation for a row in the "rushings" table
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "rushings" do
    field(:player, :string)
    field(:team, :string)
    field(:position, :string)
    field(:attempts, :integer)
    field(:attempts_per_game, :decimal)
    field(:yards, :integer)
    field(:average, :decimal)
    field(:yards_per_game, :decimal)
    field(:touchdowns, :integer)
    field(:longest_rush, :integer)
    field(:longest_rush_touchdown, :boolean, default: false)
    field(:first_downs, :integer)
    field(:first_downs_percentage, :decimal)
    field(:yards_20_plus, :integer)
    field(:yards_40_plus, :integer)
    field(:fumbles, :integer)
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, required_fields())
    |> validate_required(required_fields())
    |> validate_first_downs_percentage()
  end

  defp required_fields, do: __schema__(:fields) -- generated_fields()

  defp generated_fields, do: [:id]

  defp validate_first_downs_percentage(changeset) do
    struct = apply_changes(changeset)

    cond do
      is_nil(struct.first_downs_percentage) ->
        changeset

      struct.first_downs_percentage > Decimal.from_float(100.0) ->
        message = "cannot be greater than 100.0"
        add_error(changeset, :first_downs_percentage, message)

      Decimal.negative?(struct.first_downs_percentage) ->
        message = "cannot be less than 0.0"
        add_error(changeset, :first_downs_percentage, message)

      true ->
        changeset
    end
  end
end

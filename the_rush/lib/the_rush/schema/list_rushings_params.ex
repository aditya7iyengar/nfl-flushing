defmodule TheRush.ListRushingsParams do
  @moduledoc """
  Validator for list_rushings call
  """
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key false

  @valid_sort_fields ~w[yards longest_rush touchdowns]a
  @valid_sort_dirs ~w[asc desc]a

  embedded_schema do
    field(:page, :integer, default: 1)
    field(:page_size, :integer, default: 10)
    field(:player, :string)
    field(:sort_by, Ecto.Enum, values: @valid_sort_fields)
    field(:sort_dir, Ecto.Enum, values: @valid_sort_dirs, default: :asc)
  end

  @required_fields ~w[page page_size]a

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, __schema__(:fields))
    |> validate_required(@required_fields)
  end
end

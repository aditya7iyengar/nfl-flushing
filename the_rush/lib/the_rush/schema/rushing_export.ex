defmodule TheRush.RushingExport do
  @moduledoc """
  Ecto schema representation for a row in the "rushing_exports" table
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias TheRush.ListRushingsParams

  @valid_states ~w[new processing completed failed]a

  schema "rushing_exports" do
    field(:state, Ecto.Enum, values: @valid_states, default: :new)
    embeds_one(:params, ListRushingsParams)

    timestamps()
  end

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, [:state])
    |> cast_embed(:params, with: &params_changeset/2)
  end

  def state_changeset(struct, params) do
    cast(struct, params, [:state])
  end

  defp params_changeset(_struct, params) do
    ListRushingsParams.changeset(params)
  end
end

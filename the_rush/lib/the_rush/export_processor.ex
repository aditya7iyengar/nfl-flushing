defmodule TheRush.ExportProcessor do
  @moduledoc """
  Responsible for processing an Export (generating the final csv)
  """

  @doc """
  Process an export
  """
  def process_export(rushing_export) do
    with :ok <- validate_state(rushing_export),
         {:ok, rushing_export} <- update_state(rushing_export, :processing),
         :ok <- build_storage_dir(rushing_export),
         :ok <- write_to_csv(rushing_export) do
      update_state(rushing_export, :completed)
    else
      :skip -> :skip
      {:error, _} -> :error
    end
  end

  defp update_state(rushing_export, state) do
    TheRush.update_rushing_export_state(rushing_export, state)
  end

  @fields TheRush.Rushing.__schema__(:fields) -- [:id]
  @headers @fields
           |> Enum.map(&to_string/1)
           |> Enum.join(",")

  defp write_to_csv(rushing_export) do
    file_path = csv_file(rushing_export)
    {:ok, file} = File.open(file_path, [:write])
    IO.puts(file, @headers)

    rushing_export
    |> fetch_data()
    |> Enum.each(fn rushing ->
      write_rushing(rushing, file)
    end)

    :ok
  end

  defp fetch_data(rushing_export) do
    params = %{rushing_export.params | page: 1, page_size: nil}
    TheRush.do_list_rushings(params)
  end

  defp write_rushing(rushing, file) do
    line =
      @fields
      |> Enum.map(&Map.get(rushing, &1))
      |> Enum.join(",")

    IO.puts(file, line)
  end

  def csv_file(rushing_export) do
    rushing_export
    |> storage_dir()
    |> Path.join("output.csv")
  end

  defp build_storage_dir(rushing_export) do
    rushing_export
    |> storage_dir()
    |> File.mkdir_p()
  end

  defp storage_dir(rushing_export) do
    Application.app_dir(
      :the_rush,
      ["rushing_export", to_string(rushing_export.id)]
    )
  end

  defp validate_state(rushing_export) do
    case rushing_export.state do
      state when state in ~w[new failed]a -> :ok
      :processing -> :skip
      :completed -> :skip
    end
  end
end

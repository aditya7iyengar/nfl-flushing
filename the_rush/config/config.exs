# General application configuration
use Mix.Config

config :the_rush,
  ecto_repos: [TheRush.Repo]

# Configures the endpoint
config :the_rush, TheRushWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "TiStVvU/GzzWREvQZaHgxrjrFJWKOPpKzYIy7Rl6agFvfdl8JjxhL0+6ca3XlsYe",
  render_errors: [view: TheRushWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: TheRush.PubSub,
  live_view: [signing_salt: "AVW054Cg"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

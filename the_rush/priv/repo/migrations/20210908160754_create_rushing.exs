defmodule TheRush.Repo.Migrations.CreateRushing do
  use Ecto.Migration

  def change do
    create table(:rushings) do
      add(:player, :string, null: false)
      add(:team, :string, null: false)
      add(:position, :string, null: false)
      add(:attempts, :integer, null: false)
      add(:attempts_per_game, :decimal, null: false)
      add(:yards, :integer, null: false)
      add(:average, :decimal, null: false)
      add(:yards_per_game, :decimal, null: false)
      add(:touchdowns, :integer, null: false)
      add(:longest_rush, :integer, null: false)
      add(:longest_rush_touchdown, :boolean, null: false)
      add(:first_downs, :integer, null: false)
      add(:first_downs_percentage, :decimal, null: false)
      add(:yards_20_plus, :integer, null: false)
      add(:yards_40_plus, :integer, null: false)
      add(:fumbles, :integer, null: false)
    end

    create index(:rushings, [:player])
    create index(:rushings, [:yards])
    create index(:rushings, [:longest_rush])
    create index(:rushings, [:longest_rush_touchdown])
    create index(:rushings, [:touchdowns])
  end
end

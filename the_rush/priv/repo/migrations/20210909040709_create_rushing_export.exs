defmodule TheRush.Repo.Migrations.CreateRushingExport do
  use Ecto.Migration

  def change do
    create table(:rushing_exports) do
      add(:params, :json)
      add(:state, :string, null: false)

      timestamps()
    end

    create index(:rushing_exports, [:state])
  end
end

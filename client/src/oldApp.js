import React, { useState, useEffect } from 'react'

function buildTable(data) {
    const rushingData = data.data.map((rushing, i) => (
        <tr>
            <td key={i}>{rushing.player}</td>
            <td key={i}>{rushing.team}</td>
            <td key={i}>{rushing.position}</td>
            <td key={i}>{rushing.attempts}</td>
            <td key={i}>{rushing.attempts_per_game}</td>
            <td key={i}>{rushing.average}</td>
            <td key={i}>{rushing.first_downs}</td>
            <td key={i}>{rushing.first_downs_percentage}</td>
            <td key={i}>{rushing.fumbles}</td>
            <td key={i}>{rushing.longest_rush} {rushing.longest_rush_touchdown === true ? "T" : ""}</td>
            <td key={i}>{rushing.touchdowns}</td>
            <td key={i}>{rushing.yards}</td>
            <td key={i}>{rushing.yards_20_plus}</td>
            <td key={i}>{rushing.yards_40_plus}</td>
            <td key={i}>{rushing.yards_per_game}</td>
        </tr>
    ))
    return (
        <div class="container-sm paper">
            <table class="table-alternating flex-center">
                <thead>
                    <tr>
                        <th>Player</th>
                        <th>Team</th>
                        <th>Position</th>
                        <th>Attempts</th>
                        <th>Attempts per game</th>
                        <th>Average</th>
                        <th>First Downs</th>
                        <th>First Downs %</th>
                        <th>Fumbles</th>
                        <th>Longest Rush</th>
                        <th>Touchdowns</th>
                        <th>Yards</th>
                        <th>20+ Yards</th>
                        <th>40+ Yards</th>
                        <th>Yards per game</th>
                    </tr>
                </thead>
                <tbody>
                    { rushingData }
                </tbody>
            </table>
        </div>
    )
}

function App() {
    const path = '/api/rushings'
    const [data, setData] = useState([{}])

    useEffect(() => {
        fetch(path).then(
            result => result.json()
        ).then(
            data => {
                setData(data)
                console.log(data)
            }
        )
    }, [])

    return (
        <div>
            <div class="row flex-center">
                <h1>The Rush</h1>
            </div>

            {(typeof data.data == 'undefined') ? (
                <p>Loading</p>
            ): (
                buildTable(data)
            )}
        </div>
    )
}

export default App

import React, { Component } from 'react'
import styled from 'styled-components'
import { getRushings, exportRushings } from './Helpers'

const Container = styled.div`
  background-color: #f1f1f1;
  margin: 0;
  min-height: 100vh;
`

const Paper = styled.div`
  background-color: white;
  box-shadow: 0px 5px 20px #8c8c8c;
`

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rushings: [],
            params: {
                player: '',
                page: 1,
                page_size: 10,
                sort_by: '',
                sort_dir: 'asc'
            },
            pageInfo: {},
            errors: [],
            form: {
                player: ''
            },
            currentExport: {},
            currentExportErrors: []
        }
        this.prevPage = this.prevPage.bind(this)
        this.nextPage = this.nextPage.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleFormInputChange = this.handleFormInputChange.bind(this)
        this.updateSorts = this.updateSorts.bind(this)
        this.handleExport = this.handleExport.bind(this)
    }

    componentDidMount() {
        this.getData(this.state.params)
    }

    componentDidUpdate(_prevProps, prevState) {
        if(this.state.params !== prevState.params) {
            this.getData(this.state.params)
        }
    }

    getData(params) {
        getRushings(params).then(
            response => {
                if (typeof response.data === 'undefined') {
                    this.setState(prevState => ({
                        ...prevState,
                        rushings: [],
                        params: this.state.params,
                        pageInfo: {},
                        errors: response.errors
                    }))
                } else {
                    this.setState(prevState => ({
                        ...prevState,
                        rushings: response.data,
                        params: this.state.params,
                        pageInfo: response.page_info,
                        errors: []
                    }))
                }
            }
        )
    }

    exportData(params) {
        exportRushings(params).then(
            response => {
                if (typeof response.data === 'undefined') {
                    this.setState(prevState => ({
                        ...prevState,
                        currentExport: {},
                        currentExportErrors: response.errors
                    }))
                } else {
                    this.setState(prevState => ({
                        ...prevState,
                        currentExport: {
                            id: response.data.id,
                            state: response.data.state
                        },
                        currentExportErrors: []
                    }))
                }
            }
        )
    }

    prevPage() {
        this.updatePage(this.state.params.page - 1)
    }

    nextPage() {
        this.updatePage(this.state.params.page + 1)
    }

    updatePage(page) {
        const params = this.state.params
        this.setState(prevState => ({
            ...prevState,
            params: { ...params, page: page }
        }))
    }

    handleSubmit(e) {
        e.preventDefault()
        this.updatePlayerFilter(this.state.form.player)
    }

    updatePlayerFilter(filter) {
        const params = this.state.params
        this.setState(prevState => ({
            ...prevState,
            params: { ...params, player: filter, page: 1 }
        }))
    }

    updateSorts(e) {
        const newSortBy = e.target.name
        const params = this.state.params
        const currentSortBy = params.sort_by
        const currentSortDir = params.sort_dir
        console.log(e.target)
        if (newSortBy === currentSortBy) {
            const newParams = { ...params, sort_by: currentSortBy, sort_dir: this.toggleSortDir(currentSortDir) }
            this.setState(prevState => ({
                ...prevState,
                params: newParams
            }))
        } else {
            if (typeof newSortBy === 'undefined') {
                const newParams = { ...params, sort_by: currentSortBy, sort_dir: this.toggleSortDir(currentSortDir) }
                this.setState(prevState => ({
                    ...prevState,
                    params: newParams
                }))
            } else {
                const newParams = { ...params, sort_dir: 'asc', sort_by: newSortBy }
                this.setState(prevState => ({
                    ...prevState,
                    params: newParams
                }))
            }
        }
    }

    toggleSortDir(sortDir) {
        if (sortDir === 'asc') {
            return 'desc'
        } else {
            return 'asc'
        }
    }

    // https://stackoverflow.com/a/338334/16621059
    getSortArrows(sortBy) {
        const params = this.state.params
        const currentSortBy = params.sort_by
        const currentSortDir = params.sort_dir
        if (sortBy === currentSortBy) {
            if (currentSortDir === 'asc') {
                return '↑'
            } else {
                return '↓'
            }
        } else {
            return '↑↓'
        }
    }

    handleFormInputChange(e) {
        const value = e.target.value
        this.setState(prevState => ({
            ...prevState,
            form: { player: value }
        }));
    }

    handleExport(e) {
        const result = window.confirm("This will create an Export with current search criteria. Are you sure?");
        if (result === true) {
            this.exportData(this.state.params)
        }
    }

    tableHeader(name, title, sortable) {
        return (
            <th key={name + '-header'}>
                { title }
                <br />
                <button
                    type="button"
                    name={name}
                    onClick={ this.updateSorts }
                    className={ (sortable === true) ? "btn-small" : "disabled btn-small" }>
                    { this.getSortArrows(name) }
                </button>
            </th>
        )
    }



    render() {
        const rushings = this.state.rushings
        const pageInfo = this.state.pageInfo
        const rushingData = rushings.map((rushing, i) => (
            <tr>
                <td key={'player-' + i}>{rushing.player}</td>
                <td key={'rushing-' + i}>{rushing.team}</td>
                <td key={'position-' + i}>{rushing.position}</td>
                <td key={'attempts-' + i}>{rushing.attempts}</td>
                <td key={'attempts_per_game-' + i}>{rushing.attempts_per_game}</td>
                <td key={'average-' + i}>{rushing.average}</td>
                <td key={'first_downs-' + i}>{rushing.first_downs}</td>
                <td key={'first_downs_percentage-' + i}>{rushing.first_downs_percentage}</td>
                <td key={'fumbles-' + i}>{rushing.fumbles}</td>
                <td key={'longest_rush-' + i}>{rushing.longest_rush} {rushing.longest_rush_touchdown === true ? "T" : ""}</td>
                <td key={'touchdowns-' + i}>{rushing.touchdowns}</td>
                <td key={'yards-' + i}>{rushing.yards}</td>
                <td key={'yards_20_plus-' + i}>{rushing.yards_20_plus}</td>
                <td key={'yards_40_plus-' + i}>{rushing.yards_40_plus}</td>
                <td key={'yards_per_game-' + i}>{rushing.yards_per_game}</td>
            </tr>
        ))
        const filterFormData = (
            <form onSubmit={this.handleSubmit}>
                <div class="form-group">
                    <input
                        onChange={this.handleFormInputChange}
                        type="text"
                        placeholder="Filter by Player"
                        name="player"
                        value={this.state.form.player}
                        id="player" />
                    (Hit Enter to Search)
                </div>
            </form>
        )
        const exportData = () => {
            const exportState = this.state.currentExport.state
            const exportId = this.state.currentExport.id

            if (typeof exportId === 'undefined') {
                return (
                    <button
                        type="button"
                        onClick={ this.handleExport }>
                        Export as CSV
                    </button>
                )
            } else {
                if (exportState === 'completed') {
                    const downloadFile = () => {
                        const downloadPath = '/api/rushing_exports/' + exportId + '/download_csv'
                        fetch(downloadPath, {
                            method: 'GET'
                        }).then((response) => response.blob()).then((blob) => {
                        // Create blob link to download
                        const url = window.URL.createObjectURL(
                          new Blob([blob]),
                        );
                        const link = document.createElement('a');
                        link.href = url;
                        link.setAttribute(
                          'download',
                          `export.csv`,
                        );

                        // Append to html link element page
                        document.body.appendChild(link);

                        // Start download
                        link.click();

                        // Clean up and remove the link
                        link.parentNode.removeChild(link);
                      });
                    }
                    return (
                        <button onClick={downloadFile} >
                            Download export
                        </button>
                    )
                } else {
                    return (
                        <p>Export # { exportId } { exportState }</p>
                    )
                }
            }
        }
        return (
            <Container className="row flex-center flex-middle">
                <Paper className="border border-primary padding-large margin-large no-responsive">
                    <div class="row flex-center">
                        <h3 className="margin-small">Rushings</h3>
                    </div>
                    <hr />
                    <div class="row flex-spaces">
                        { filterFormData }
                        { exportData() }
                    </div>
                    <table class="table-alternating flex-center">
                        <thead>
                            <tr>
                                { this.tableHeader('player', 'Player', false) }
                                { this.tableHeader('team', 'Team', false) }
                                { this.tableHeader('position', 'Position', false) }
                                { this.tableHeader('attempts', 'Attempts', false) }
                                { this.tableHeader('attempts_per_game', 'Attempts/Game', false) }
                                { this.tableHeader('average', 'Average', false) }
                                { this.tableHeader('first_downs', 'First Downs', false) }
                                { this.tableHeader('first_downs_percentage', 'First Downs', false) }
                                { this.tableHeader('fumbles', 'Fumbles', false) }
                                { this.tableHeader('longest_rush', 'Longest Rush', true) }
                                { this.tableHeader('touchdowns', 'Touchdowns', true) }
                                { this.tableHeader('yards', 'Yards', true) }
                                { this.tableHeader('yards_20_plus', '20+ Yards', false) }
                                { this.tableHeader('yards_40_plus', '40+ Yards', false) }
                                { this.tableHeader('yards_per_game', 'Yards/Game', false) }
                            </tr>
                        </thead>
                        <tbody>
                            { rushingData }
                        </tbody>
                    </table>
                    <br />
                    <div class="row flex-center">
                        <div className="col align-middle">
                            <button
                                className={ (pageInfo.page <= 1) ? "disabled" : "" }
                                type="button"
                                onClick={ this.prevPage }>
                                &larr; Prev
                            </button>
                            &nbsp; &nbsp;
                            Page { pageInfo.page } of { pageInfo.total_pages }
                            &nbsp; &nbsp;
                            <button
                                className={ (pageInfo.page >= pageInfo.total_pages) ? "disabled" : "" }
                                type="button"
                                onClick={ this.nextPage }>
                                Next &rarr;
                            </button>
                        </div>
                    </div>
                </Paper>
            </Container>
        )
    }
}

export default App

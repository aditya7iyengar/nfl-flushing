export const getRushings = (params) => {
    let url = new URL('http://localhost:4000/api/rushings')
    let searchParams = new URLSearchParams(params)
    // remove empty params
    searchParams.forEach((v, k) => {
        if (v === '')
            searchParams.delete(k);
    });
    url.search = searchParams
    console.log(params)
    let result = fetch(url.pathname + url.search).then( result =>  {
        return result.json()
    })
    return result
}


export const exportRushings = (params) => {
    let url = new URL('http://localhost:4000/api/rushing_exports/sync_create')
    let searchParams = new URLSearchParams(params)
    // remove empty params
    searchParams.forEach((v, k) => {
        if (v === '')
            searchParams.delete(k);
    });
    url.search = searchParams
    console.log(params)
    let result = fetch(url.pathname + url.search, {method: 'POST'}).then( result =>  {
        console.log(result)
        return result.json()
    })
    return result
}

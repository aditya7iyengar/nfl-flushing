run:
	# Set up the database with seeds
	docker-compose run backend mix ecto.create
	docker-compose run backend mix ecto.migrate
	docker-compose run backend mix run priv/repo/seeds.exs
	# Run app
	docker-compose up
